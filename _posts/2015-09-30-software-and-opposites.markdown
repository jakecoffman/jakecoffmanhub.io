---
layout:     post
title:      "software paradigm extremism"
subtitle:   "or why love is not the opposite of hate"
header-img: "img/post-bg-05.jpg"
---

I like talking about opposites. Sometimes I randomly will ask unassuming
victims/coworkers what they think the opposite of "love" is. Go ahead,
what do you think is the opposite of love? Is it hate? Indifference or apathy?

With your answer in mind, now what is the opposite of "like" as in you like
a person. Maybe dislike or indifference again? Most people in their mind have
a sliding scale like this:

<center><pre>
<------------------------------------------------->
 |        |           |             |          |
love     like     indifferent     dislike     hate
</pre></center>

I actually think this is wrong. I think the opposite of each of these is
just the absence of the thing. My scales would look like this:

<center><pre>
<------------------------------------------------->
 |                                             |
love                                        not love
</pre></center>

<center><pre>
<------------------------------------------------->
 |                                             |
indifference                              not indifference
</pre></center>

You get the picture, not hard to buy.

So what does this have to do with software? Software paradigms are often
taken to the extreme where we, for example, go pure OOP or pure functional
thinking we are on a scale like this:

<center><pre>
<------------------------------------------------->
 |                                             |
 OOP                                        functional
</pre></center>

These paradigms are not opposites of each other, and they should both be
incorporated into our programs along with every other paradigm. We should
be taking advantage of each paradigm's strength while avoiding their weaknesses.

Languages or people that force us into thinking there is one true paradigm
have historically been wrong. While each paradigm brings some enlightenment,
no single paradigm solves all problems!

So use a language that puts the power in your hands and learn best practices
on where to put state and where to use objects.

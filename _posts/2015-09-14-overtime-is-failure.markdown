---
layout:     post
title:      "overtime is failure"
subtitle:   "if I have to work more than 8 hours, someone screwed up"
header-img: "img/post-bg-02.jpg"
---

In the corporate world you often hear stories of people working overtime
to make themselves look good. Raises are coming up soon, better stay late!
Get more done!

Not quite. With "knowledge workers" and especially software developers,
more time worked does not mean more productivity. Especially in the long run.

It's hard to get in the mindset of CEOs/managers that think their employees
that work overtime are more valuable than those that don't. Imagine you have
two developers, one works 8 hour days and the other stays there all night.
Both do roughly the same amount of work. Why reward the guy burning himself out?

Overtime does sometimes need to be done, like when a critical bug is released,
a server goes down, or the company could lose/gain an important customer if you
just pushed through a few long days.

In those cases, overtime should be punished or rewarded. If it's a mistake from
someone in the company, that person should work to figure out how to avoid
that situation again as punishment. Those that do the overtime should be
rewarded by getting equal time off, and maybe some cash for their vacation.

For startups the story is similar but often overtime is more justified. In
most cases it should be avoided to give people a chance to think about what
they're doing. However, sometimes though there is a customer demo that just needs to be
pounded out. We're getting to the end of that runway after all!

At the end of the day though, there's nothing like a good night's sleep
to get over the writer's block at work.
